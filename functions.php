<?php
/**
 * This is the child theme for digicrew theme.
 */

/* social icons walker */
require( dirname( __FILE__ ) . '/core/class_nav_social_walker.php' );

function digicrew_lite_theme_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
	    wp_enqueue_style( 'child-style',
	        get_stylesheet_directory_uri() . '/style.css',
	        array('parent-style')
	    );
}

add_action( 'wp_enqueue_scripts', 'digicrew_lite_theme_styles' );

add_action( 'after_setup_theme', 'digicrew_lite_setup' );
function digicrew_lite_setup() {  
		
		register_nav_menus(
	        array(
	            'social' => __('Social Icon Menu Area', 'digicrew-lite')
	        )
	    );
}