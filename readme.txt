=== Digicrew Lite ===

Contributors: weblizar
Tags: custom-background, custom-logo, custom-menu, featured-images, threaded-comments , custom-header , footer-widgets , sticky-post , right-sidebar , two-columns
Requires at least: 5.0
Requires PHP: 5.6
Tested up to: 5.8
Stable tag: 1.0
License: GNU General Public License v2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==

Digicrew-Lite WordPress free blog theme.

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload Theme and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Copyright ===

* Digicrew Lite WordPress Free Theme, Copyright 2021
* Digicrew Lite theme is distributed under the terms of the GNU GPL

== Images License ==

Image for screenshot
License: Creative Commons CC0 license.
License https://pxhere.com/en/license
Source: https://pxhere.com/en/photo/723003

== Changelog ==
= 1.0 - August 24 2021 =
* Initial Submission
