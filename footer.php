<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package digicrew-lite
 */

?>

	<!-- Footer-section -->
	<footer class="footer-bg">
	    <?php  if (is_active_sidebar('footer-widget-area')) { ?>
	        <div class="ws-section-spacing">
	            <div class="container">
	                <div class="row">
	                    <?php dynamic_sidebar('footer-widget-area'); ?>
	                </div>
	            </div>
	        </div>
	    <?php } ?>
	    <div class="copyright-area">
	        <div class="container">
	            <div class="row">
	                <div class="col-lg-6">
	                    <p>
                        	<?php echo esc_html( gmdate( 'Y' ) ); ?> <?php echo esc_html( get_bloginfo( 'name' ) ); ?> <?php echo esc_html__( 'All rights reserved.', 'digicrew-lite' ); ?>
                        </p>
	                </div>
					<div class="col-lg-6">
						<div class="social center-social">
							<?php if ( has_nav_menu( 'social' ) ) : 
							wp_nav_menu(
								array(
									'theme_location' => 'social',
									'menu_class'     => 'social-network ',
									'walker' => new WO_Nav_Social_Walker(),
									'depth'          => 1,
									'link_before'    => '<span class="screen-reader-text">',
									'link_after'     => '</span>',
								)
							);
							endif; ?>
						</div>
					</div>
	            </div>
	        </div>
	    </div>
	</footer>
	<!-- End-footer-section -->
</div><!-- #page -->
<a id="btn-to-top"></a>
<?php wp_footer(); ?>
</body>
</html>